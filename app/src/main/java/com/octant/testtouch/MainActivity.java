package com.octant.testtouch;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import static android.R.attr.button;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {

    RelativeLayout mainLayout;
    MyButton btn_hello;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainLayout = (RelativeLayout)findViewById(R.id.al_main);
        mainLayout.setOnTouchListener(this);
        btn_hello = (MyButton)findViewById(R.id.btn_hello);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()){
            case MotionEvent.ACTION_DOWN:{
                /*AbsoluteLayout.LayoutParams params=(AbsoluteLayout.LayoutParams)btn_hello.getLayoutParams();
                float x = motionEvent.getRawX();
                float y = motionEvent.getRawY();
                params.x = (int)x-mainLayout.getLeft()-params.height/2;
                params.y = (int)y-mainLayout.getTop()-params.width/2;
                btn_hello.setLayoutParams(params);
                btn_hello.setVisibility(View.VISIBLE);*/
                RelativeLayout.LayoutParams params=(RelativeLayout.LayoutParams)btn_hello.getLayoutParams();
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                int btn_width = btn_hello.getRight()-btn_hello.getLeft();
                int btn_height = btn_hello.getBottom()-btn_hello.getTop();
                params.setMargins((int)x-btn_width/2,(int)y-btn_height/2,0,0);
                btn_hello.setLayoutParams(params);
                btn_hello.setVisibility(View.VISIBLE);
            }break;
            case MotionEvent.ACTION_UP:{
                btn_hello.setVisibility(View.GONE);
                Log.i("MainActivity","onTouch is up,set gone");
            }break;
            default:
        }
        return true;
    }
}
